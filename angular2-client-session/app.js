var express = require('express');
var path = require('path');
var cookieParser = require('socket.io-cookie');
/*create via express*/
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

app.get('/', function(req, res, next){
  res.sendFile(__dirname + '/index.html');
});

/*socket io server code*/ 
io.use(cookieParser);
io.use(function (socket, next) {
  console.log(socket.request.headers.cookie.sessionid);
  next();
});
io.on('connection', function(socket){
  console.log('a user connected');
  io.emit('notify', 'you connected me');
});


/*server static express*/
app.use(express.static(path.join(__dirname, 'public')));
app.use('/node_modules', express.static(__dirname + '/node_modules'));


module.exports = {app: app, server: server};